﻿using System.Data.SqlClient;

namespace WebExperience.Test.Models
{
    public class QueryOptions
    {
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public string SortField { get; set; }
        public SortOrder SortOrder { get; set; }
        public string Sort
        {
            get {
                return string.Format("{0} {1}", SortField, SortOrder.ToString());
            }
        }

        public QueryOptions()
        {
            CurrentPage = 1;
            PageSize = 5;
            SortField = "AssetId";
            SortOrder = SortOrder.Ascending;
        }
    }
}