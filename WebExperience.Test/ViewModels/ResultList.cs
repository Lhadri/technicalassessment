﻿using System.Collections.Generic;

using Newtonsoft.Json;

using WebExperience.Test.Models;

namespace WebExperience.Test.ViewModels
{
    public class ResultList<T>
    {
        [JsonProperty(PropertyName = "queryOptions")]
        public QueryOptions QueryOptions { get; set; }

        [JsonProperty(PropertyName = "results")]
        public List<T> Results { get; set; }
    }
}