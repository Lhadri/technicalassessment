﻿using System;

using Newtonsoft.Json;

namespace WebExperience.Test.ViewModels
{
    public class AssetViewModel
    {
        [JsonProperty(PropertyName = "assetId")]
        public Guid AssetId { get; set; }
        [JsonProperty(PropertyName = "fileName")]
        public string FileName { get; set; }
        [JsonProperty(PropertyName = "mimeType")]
        public string MimeType { get; set; }
        [JsonProperty(PropertyName = "createdBy")]
        public string CreatedBy { get; set; }
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
    }
}