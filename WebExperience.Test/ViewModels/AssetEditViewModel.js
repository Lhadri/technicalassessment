﻿function AssetEditViewModel(asset) {

    var self = this;

    self.saveCompleted = ko.observable(false);

    self.sending = ko.observable(false);

    self.asset = {
        assetId: asset.assetId,
        fileName: ko.observable(asset.fileName),
        mimeType: ko.observable(asset.mimeType),
        createdBy: ko.observable(asset.createdBy),
        email: ko.observable(asset.email),
        country: ko.observable(asset.country),
        description: ko.observable(asset.description)
    };

    //function introduces the submit data binding and is
    //called by Knockout when the assets form is submitted
    self.edit = function (form) {
        self.sending(true);

        // include the anti forgery token
        self.asset.__RequestVerificationToken = form[0].value;

        $.ajax({
            url: '/api/Assets',
            type: 'put',
            contentType: 'application/json',
            data: ko.toJSON(self.asset),
            success: function () {
                self.saveCompleted(true);

                $('.body-content').prepend(
                    '<div class="alert alert-success"> <strong>Success!</strong> The new asset has been <strong>updated</strong>.</div> ');
                setTimeout(function () { location.href = '../'; }, 2000);
            },
            error: function () {
                $('.body-content').prepend(
                    '<div class="alert alert-danger"> <strong> Error!</strong> There was an error updating the asset.</div> ');
            },
            complete: function () { self.sending(false) }
        })
    };
}