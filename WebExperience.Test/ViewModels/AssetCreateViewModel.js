﻿function AssetCreateViewModel() {
    var self = this;

    self.saveCompleted = ko.observable(false);

    self.sending = ko.observable(false);

    self.asset = {
        fileName: ko.observable(),
        mimeType: ko.observable(),
        createdBy: ko.observable(),
        email: ko.observable(),
        country: ko.observable(),
        description: ko.observable(),
    };

    //function introduces the submit data binding and is
    //called by Knockout when the assets form is submitted
    self.save = function (form) {
        self.sending(true);

        // include the anti forgery token
        self.asset.__RequestVerificationToken = form[0].value;
        $.ajax({
            url: '/api/Assets',
            type: 'post',
            contentType: 'application/json',
            data: ko.toJSON(self.asset),
            success: function () {
                self.saveCompleted(true);

                $('.body-content').prepend(
                    '<div class="alert alert-success"> <strong>Success!</strong> The new asset has been <strong>created</strong>.</div> ');
                setTimeout(function () { location.href = './'; }, 2000);
            },
            error: function () {
                $('.body-content').prepend(
                    '<div class="alert alert-danger"> <strong> Error!</strong> There was an error creating the asset.</div> ');
            },
            complete: function () { self.sending(false) }
        })
    };
}