﻿using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;

namespace WebExperience.Test.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static HtmlString HtmlConvertToJson(this HtmlHelper htmlHelper, object model)
        {
            var settings = new JsonSerializerSettings {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore ,
                Formatting = Formatting.Indented
            };

            return new HtmlString(JsonConvert.SerializeObject(model, settings));
        }

        public static MvcHtmlString BuildNextPreviousLinks(this HtmlHelper htmlHelper, string actionName)
        {
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

            return new MvcHtmlString(string.Format(
                "<nav>" +
                    " <ul class=\"pager\">" +
                        "<li data-bind=\"css: pagingService.buildPreviousClass()\">" +
                            "<a href=\"{0}\" data-bind=\"click: pagingService.previousPage\"> Previous </a ></li>" +
                        "<li data-bind=\"css: pagingService.buildNextClass()\">" +
                            "<a href=\"{0}\" data-bind=\"click: pagingService.nextPage\"> Next </a></li>" +
                    "</ul>" +
                "</nav>",
                @urlHelper.Action(actionName)));
        }
    }
}