﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

using WebExperience.Test.Models;
using WebExperience.Test.ViewModels;

namespace WebExperience.Test.Controllers.Api
{
    public class AssetsController : ApiController
    {
        private AssetContext db = new AssetContext();

        // GET: api/Assets
        public ResultList<AssetViewModel> Get([FromUri] QueryOptions queryOptions)
        {
            var start = (queryOptions.CurrentPage - 1) * queryOptions.PageSize;

            var assets = db.Assets
                .OrderBy(queryOptions.Sort)
                .Skip(start)
                .Take(queryOptions.PageSize);

            queryOptions.TotalPages = (int)Math.Ceiling((double)db.Assets.Count() / queryOptions.PageSize);

            AutoMapper.Mapper.CreateMap<Asset, AssetViewModel>();

            return new ResultList<AssetViewModel> {
                QueryOptions = queryOptions,
                Results = AutoMapper.Mapper.Map<List<Asset>, List<AssetViewModel>>(assets.ToList())
            };
        }

        // PUT(edit): api/Assets/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Put(AssetViewModel asset)
        {
            if(!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            AutoMapper.Mapper.CreateMap<AssetViewModel, Asset>();
            db.Entry(AutoMapper.Mapper.Map<AssetViewModel, Asset>(asset)).State = EntityState.Modified;
            db.SaveChanges();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST(create): api/Assets
        [ResponseType(typeof(AssetViewModel))]
        public IHttpActionResult Post(AssetViewModel asset)
        {
            if(!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            asset.AssetId = Guid.NewGuid();
            AutoMapper.Mapper.CreateMap<AssetViewModel, Asset>();
            db.Assets.Add(AutoMapper.Mapper.Map<AssetViewModel, Asset>(asset));
            db.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { id = asset.AssetId }, asset);
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
