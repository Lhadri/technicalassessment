﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web.ModelBinding;
using System.Web.Mvc;

using WebExperience.Test.Models;
using WebExperience.Test.ViewModels;

namespace WebExperience.Test.Controllers
{
    public class AssetsController : Controller
    {
        private AssetContext db = new AssetContext();

        // GET: Assets
        public ActionResult Index([Form] QueryOptions queryOptions)
        {
            var start = (queryOptions.CurrentPage - 1) * queryOptions.PageSize;

            var assets = db.Assets
                .OrderBy(queryOptions.Sort)
                .Skip(start)
                .Take(queryOptions.PageSize);

            queryOptions.TotalPages = (int)Math.Ceiling((double)db.Assets.Count() / queryOptions.PageSize);

            AutoMapper.Mapper.CreateMap<Asset, AssetViewModel>();

            return View(new ResultList<AssetViewModel> {
                QueryOptions = queryOptions,
                Results = AutoMapper.Mapper.Map<List<Asset>, List<AssetViewModel>>(assets.ToList())
            });
        }

        // GET: Assets/Details/5
        public ActionResult Details(Guid? id)
        {
            if(id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Asset asset = db.Assets.Find(id);
            if(asset == null) {
                return HttpNotFound();
            }
            return View(asset);
        }

        // GET: Assets/Create
        public ActionResult Create()
        {
            return View(new AssetViewModel());
        }

        // GET: Assets/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if(id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Asset asset = db.Assets.Find(id);
            if(asset == null) {
                return HttpNotFound();
            }

            AutoMapper.Mapper.CreateMap<Asset, AssetViewModel>();
            return View(AutoMapper.Mapper.Map<Asset, AssetViewModel>(asset));
        }

        // GET: Assets/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if(id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Asset asset = db.Assets.Find(id);
            if(asset == null) {
                return HttpNotFound();
            }

            AutoMapper.Mapper.CreateMap<Asset, AssetViewModel>();
            return View(AutoMapper.Mapper.Map<Asset, AssetViewModel>(asset));
        }

        // POST: Assets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Asset asset = db.Assets.Find(id);
            db.Assets.Remove(asset);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
