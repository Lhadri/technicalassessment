﻿using System;

namespace GeneralKnowledge.Test.App.Models {
    public class Sample {

        public DateTime Date { get; set; }
        public float Temperature { get; set; }
        public int Ph { get; set; }
        public int Phosphate { get; set; }
        public int Chloride { get; set; }
        public int Nitrate { get; set; }


        public Sample[] SampleItems = { };


    }
}
