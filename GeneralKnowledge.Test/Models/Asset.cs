﻿namespace GeneralKnowledge.Test.App.Models {

    public class Asset {
        public string Country { get; set; }
        public string Mime_Type { get; set; }
    }
}
