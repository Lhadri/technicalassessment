﻿using GeneralKnowledge.Test.App.Models;

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.DAL {
    public class AssetContext: DbContext {

        public AssetContext() { }
        public AssetContext(DbConnection connection) : base(connection ,true) {
            //this.Configuration.LazyLoadingEnabled = false;
            //this.Configuration.ProxyCreationEnabled = false;
        }
        public DbSet<Asset> Assets { get; set; }
    }
}
