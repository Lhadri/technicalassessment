﻿using System.Linq;
using System.Text;

using ConsoleTables;

using Newtonsoft.Json.Linq;


namespace GeneralKnowledge.Test.App.Tests
{
    public class JsonReadingTest : ITest
    {
        public string Name { get { return "JSON Reading Test"; } }

        public void Run()
        {
            var jsonData = Resources.SamplePoints;
            PrintOverview(jsonData);
        }

        private void PrintOverview(byte[] data)
        {
            string str = Encoding.UTF8.GetString(data ,0 ,data.Length);
            JObject json = JObject.Parse(str);

            var tempSamples = json["samples"].Select(e => e.SelectToken("temperature")?.Value<float>());
            var phSamples = json["samples"].Select(e => e.SelectToken("pH")?.Value<int>());
            var phosphSamples = json["samples"].Select(e => e.SelectToken("phosphate")?.Value<int>());
            var chlorSamples = json["samples"].Select(e => e.SelectToken("chloride")?.Value<int>());
            var nitrSamples = json["samples"].Select(e => e.SelectToken("nitrate")?.Value<int>());

            var table = new ConsoleTable("Parameter" ,"LOW" ,"AVG" ,"MAX");
            table.AddRow("Temperature", tempSamples.Min(), string.Format("{0:0.##}", tempSamples.Average()), tempSamples.Max())
                 .AddRow("PH", phSamples.Min(), string.Format("{0:0.##}", phSamples.Average()), phSamples.Max())
                 .AddRow("Chloride", phosphSamples.Min(), string.Format("{0:0.##}", phosphSamples.Average()), phosphSamples.Max())
                 .AddRow("Phosphate", chlorSamples.Min(), string.Format("{0:0.##}", chlorSamples.Average()), chlorSamples.Max())
                 .AddRow("Nitrate", nitrSamples.Min(), string.Format("{0:0.##}", nitrSamples.Average()), nitrSamples.Max());
            table.Write();
        }
    }
}
