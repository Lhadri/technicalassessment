﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

using CsvHelper;
using CsvHelper.Configuration;

using GeneralKnowledge.Test.App.Models;

namespace GeneralKnowledge.Test.App.Tests
{
    public class CsvProcessingTest : ITest
    {
        public void Run()
        {
            var csvFile = Resources.AssetImport;

            byte[] byteArray = Encoding.UTF8.GetBytes(csvFile);
            MemoryStream stream = new MemoryStream(byteArray);

            // using the library
            var config = new CsvConfiguration(CultureInfo.InvariantCulture) {
                PrepareHeaderForMatch = args => args.Header.ToLower() ,
            };

            using(var reader = new StreamReader(stream))
            using(var csv = new CsvReader(reader, config)) {
                var records = csv.GetRecords<Asset>();
                foreach(var item in records) {
                    Console.WriteLine($"{item.Country} --- {item.Mime_Type}");
                }
            }
        }
    }
}
