﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;

namespace GeneralKnowledge.Test.App.Tests
{
    public class RescaleImageTest : ITest
    {
        // For example: 100x80 (thumbnail) and 1200x1600 (preview)
        int MaxWidth = 100;
        int MaxHeight = 80;

        public void Run()
        {
            // download image
            WebClient wc = new WebClient();
            Stream stream = wc.OpenRead("https://homepages.cae.wisc.edu/~ece533/images/fruits.png");
            Bitmap image = new Bitmap(stream);

            // rescale image
            var ratioX = (double)MaxWidth / image.Width;
            var ratioY = (double)MaxHeight / image.Height;
            var ratio = Math.Min(ratioX ,ratioY);
            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);
            var newImage = new Bitmap(newWidth ,newHeight);
            using(var graphics = Graphics.FromImage(newImage))
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);

            // save image
            newImage.Save("fruits.jpg", ImageFormat.Png);

            // show image
            ProcessStartInfo psi = new ProcessStartInfo {
                FileName = "mspaint.exe",
                Arguments = "fruits.jpg"
            };

            Process.Start(psi);
        }
    }
}
