﻿using System;
using System.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    public class StringTests : ITest
    {
        public void Run()
        {
            AnagramTest();
            GetUniqueCharsAndCount();
        }

        private void AnagramTest()
        {
            var word = "stop";
            var possibleAnagrams = new string[] { "test" ,"tops" ,"spin" ,"post" ,"mist" ,"step" };

            foreach(var possibleAnagram in possibleAnagrams) {
                Console.WriteLine(string.Format("{0} > {1}: {2}", word, possibleAnagram, possibleAnagram.IsAnagram(word)));
            }
        }

        private void GetUniqueCharsAndCount()
        {
            var word = "xxzwxzyzzyxwxzyxyzyxzyxzyzyxzzz";
            char[] charArray = word.ToCharArray();
            var query = charArray
                .GroupBy(chr => chr)
                .Select(chr => new {
                    Character = chr.Key ,
                    Count = chr.Count()
                });

            foreach(var item in query) {
                Console.WriteLine($"{item.Character} --> {item.Count}");
            }
        }
    }

    public static class StringExtensions
    {
        public static bool IsAnagram(this string a, string b)
        {
            if(a.Length != b.Length)
                return false;

            char[] charArray_a = a.ToCharArray();
            Array.Sort(charArray_a);
            a = new string(charArray_a);

            char[] charArray_b = b.ToCharArray();
            Array.Sort(charArray_b);
            b = new string(charArray_b);

            return string.Equals(a, b, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
